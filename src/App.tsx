import * as React from 'react';
import {ChangeEvent, Fragment} from 'react';
import './App.css';



class App extends React.Component {
    constructor(props: any) {
        super(props);
        this.state = {
            formData: {
                'e-mail': '',
                name: '',
                password: '',
            },
            formErrors: {},
        };

        this.onChange = this.onChange.bind(this);
    }

    public onChange(event: ChangeEvent<HTMLInputElement>) {

        console.log(event.target.name, event.target.value)
    }

    public render() {
        return (
            <Fragment>
                <form className="form" action="submit">
                    <label htmlFor="name">
                        name
                        <input onChange={this.onChange} autoComplete="off" type="text" name="name"/>
                    </label>
                    <label htmlFor="password">
                        password
                        <input onChange={this.onChange} autoComplete="off" type="text" name="password"/>
                    </label>
                    <label htmlFor="e-mail">
                        e-mail
                        <input onChange={this.onChange} autoComplete="off" type="text" name="e-mail"/>
                    </label>
                </form>
            </Fragment>
        );
    }
}

export default App;
